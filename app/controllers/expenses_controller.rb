class ExpensesController < ApplicationController
	def index
		@expenses = Expense.all
		@user = User.find(params[:user_id])
 
		respond_to do |format|
			format.html  # index.html.erb
			format.json { render json: @expenses }
  		end
  	end

	def new
		@expense = Expense.new
		@user = User.find(params[:user_id])
 
	  	respond_to do |format|
	    	format.html  # new.html.erb
	  	end
	end

  	def create
  		@user = User.find(params[:user_id])
		@expense = @user.expenses.create(params[:expense])
		redirect_to user_expenses_path(current_user)
	end

	def show
		@expense = Expense.find(params[:id])
	 
	  	respond_to do |format|
	    	format.html  # show.html.erb
	  	end
	end

	def edit
    	@expense = Expense.find(params[:id])
    	@user = User.find(params[:user_id])
  	end

  	def update
	    @expense = Expense.find(params[:id])

	    respond_to do |format|
	      	if @expense.update_attributes(params[:expense])
	        	format.html { redirect_to @expense, notice: 'Expense was successfully updated.' }
	        	format.json { head :no_content }
	      	else
	        	format.html { render action: "edit" }
	        	format.json { render json: @expense.errors, status: :unprocessable_entity }
	      	end
	    end
	end

  	def destroy
  		@user = User.find(params[:user_id])
		@expense = @user.expenses.find(params[:id])
		@expense.destroy
		redirect_to user_expenses_path(current_user)
	end
end
