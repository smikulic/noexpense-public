class MainController < ApplicationController
  def index
  	@balance = current_user.incomes.sum('value') - current_user.expenses.sum('value')
  end
end
