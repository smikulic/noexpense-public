class IncomesController < ApplicationController
  def index
		@incomes = Income.all
		@user = User.find(params[:user_id])
 
		respond_to do |format|
			format.html  # index.html.erb
			format.json { render json: @incomes }
  		end
  	end

  	def new
		@income = Income.new
		@user = User.find(params[:user_id])
 
	  	respond_to do |format|
	    	format.html  # new.html.erb
	  	end
	end

	def create
  		@user = User.find(params[:user_id])
		@income = @user.incomes.create(params[:income])
		redirect_to user_incomes_path(current_user)
	end

	def show
		@income = Income.find(params[:id])
	 
	  	respond_to do |format|
	    	format.html  # show.html.erb
	  	end
	end

	def edit
    	@income = Income.find(params[:id])
    	@user = User.find(params[:user_id])
  	end

  	def update
    	@income = Income.find(params[:id])

	    respond_to do |format|
	    	if @income.update_attributes(params[:income])
	        	format.html { redirect_to @income, notice: 'Income was successfully updated.' }
	        	format.json { head :no_content }
	      	else
	        	format.html { render action: "edit" }
	        	format.json { render json: @income.errors, status: :unprocessable_entity }
	      	end
	    end
  	end

  	def destroy
    	@user = User.find(params[:user_id])
		@income = @user.incomes.find(params[:id])
		@income.destroy
		redirect_to user_incomes_path(current_user)
	end
end
