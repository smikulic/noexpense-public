class Income < ActiveRecord::Base
	belongs_to :user
  	attr_accessible :income_date, :name, :value

  	validates :name,  :presence => true
  	validates :value, :presence => true
end
