class Expense < ActiveRecord::Base
	belongs_to :user
  	attr_accessible :name, :value, :expense_date

  	validates :name,  :presence => true
  	validates :value, :presence => true
end