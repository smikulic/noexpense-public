# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130704143207) do

  create_table "expenses", :force => true do |t|
    t.string   "name"
    t.decimal  "value"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.datetime "expense_date"
    t.integer  "user_id"
  end

  add_index "expenses", ["user_id"], :name => "index_expenses_on_user_id"

  create_table "incomes", :force => true do |t|
    t.string   "name"
    t.decimal  "value"
    t.datetime "income_date"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "user_id"
  end

  add_index "incomes", ["user_id"], :name => "index_incomes_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "currency"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.decimal  "total_expenses"
  end

end
