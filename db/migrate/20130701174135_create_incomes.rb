class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.string :name
      t.decimal :value
      t.datetime :income_date

      t.timestamps
    end
  end
end
