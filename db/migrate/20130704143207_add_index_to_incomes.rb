class AddIndexToIncomes < ActiveRecord::Migration
  def change
		change_table :incomes do |t|
      		t.references :user 
    	end
    	add_index :incomes, :user_id
  	end
end
