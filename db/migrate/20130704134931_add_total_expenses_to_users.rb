class AddTotalExpensesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :total_expenses, :decimal
  end
end
