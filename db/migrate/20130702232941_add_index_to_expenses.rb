class AddIndexToExpenses < ActiveRecord::Migration
	def change
		change_table :expenses do |t|
      		t.references :user 
    	end
    	add_index :expenses, :user_id
  	end
end
